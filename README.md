[![XAMPP version](https://img.shields.io/badge/XAMPP-8.1.6-1abc9c.svg)](https://www.apachefriends.org/)  [![Gitter](https://badges.gitter.im/docker-xampp/community.svg)](https://gitter.im/docker-xampp/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

| PHP version | Corresponding tag |
--------------|---------------------
| 7.4.29 | `ictpcexpress/rockitpinoylampp:7` |

For PHP 7, start your container like this:
```bash
docker run --name mylocalserver -p 57061:22 -p 57069:80 -d -v ~/my_web_pages:/www ictpcexpress/rockitpinoylampp:7
```

docker-xampp
===

This image is intended for PHP+MySQL development. For convenience, it also runs SSH server to connect to. __Both MySQL and phpmyadmin use default XAMPP password__.

## Any questions?

Please read this readme before asking anything.

If you didn't find answer to your question, [send an email](rockitpinoy.ph@gmail.com). I'll try to help if I can!

## Running the image

This image uses /www directory for your page files, so you need to mount it.

```
docker run --name mylocalserver -p 57066:22 -p 57069:80 -d -v ~/my_web_pages:/www ictpcexpress/rockitpinoylampp:7
```
The command above will expose the SSH server on port 57066 and HTTP server on port 57069.
Feel free to use your own name for the container.

To browse to your web page, visit this URL: [http://mylocalserver.pcx.com.ph:57069/www](http://mylocalserver.pcx.com.ph:57069/www)
And to open up the XAMPP interface: [http://mylocalserver.pcx.com.ph:57069/](http://mylocalserver.pcx.com.ph:57069/)

## Default credentials

service | username | password
------- | -------- | ---------
ssh     | root     | rockitpinoy

## Additional How-tos

### My app can't function in `/www` folder

No problem, just mount your app in the `/opt/lampp/htdocs` folder, for example:

```
docker run --name mylocalserver -p 57066:22 -p 57069:80 -d -v ~/my_web_pages:/opt/lampp/htdocs ictpcexpress/rockitpinoylampp:7
```

### ssh connection

default SSH password is 'rockitpinoy'.

```
ssh root@localhost -p 57066
```

### get a shell terminal inside your container

```
docker exec -ti mylocalserver bash
```

### use binaries provided by XAMPP

Inside docker container:
```
export PATH=/opt/lampp/bin:$PATH
```
You can then use `mysql` and friends installed in `/opt/lampp/bin` in your current bash session. If you want this to persist, you need to add it to your user or system-wide `.bashrc` (inside container).

### Use your own configuration

In your home directory, create a `my_apache_conf` directory in which you place any number of apache configuration files. As soon as they end with the `.conf` extension, they will be used by xampp. Make sure to use the following flag in your command: `-v ~/my_apache_conf:/opt/lampp/apache2/conf.d`, for example:

```
docker run --name mylocalserver -p 57066:22 -p 57069:80 -d -v ~/my_web_pages:/www  -v ~/my_apache_conf:/opt/lampp/apache2/conf.d ictpcexpress/rockitpinoylampp:7
```

### Restart the server

Once you have modified configuration for example
```
docker exec mylocalserver /opt/lampp/lampp restart
```

### phpmyadmin

If you used the flag `-p 57069:80` with `docker run`, just browse to http://mylocalserver.pcx.com.ph:57069/phpmyadmin/ .

## Use a Different XAMPP or PHP Version

Currently, the Docker image is built only for PHP 5, 7 and 8.
If you need another version, you can easily build a Docker image yourself, here's how:

1. Clone this repo
2. Find the URL to a URL to your desired version. List of versions can be found here: https://sourceforge.net/projects/xampp/files/XAMPP%20Linux/
3. Build the image using e.g. `docker build --build-arg XAMPP_URL="https://www.apachefriends.org/xampp-files/5.6.40/xampp-linux-x64-5.6.40-1-installer.run?from_af=true" .`


## Additional config for your windows client

C:\Windows\System32\drivers\etc\hosts
## Add this line
127.0.0.1 	mylocalserver.pcx.com.ph

